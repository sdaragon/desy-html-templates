** Notice: This project is deprecated. It is replaced with this one: [desy-html-starter](https://bitbucket.org/sdaragon/desy-html-starter/) and the documentation in [desy.aragon.es](https://bitbucket.org/sdaragon/desy.aragon.es/). We keep this project just for historical purposes, but will no longer be maintained. **

# desy-html-templates
![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)
![Prerrequisito](https://img.shields.io/badge/npm-%3E%3D6.14.0-blue.svg)
![Prerrequisito](https://img.shields.io/badge/node-%3E%3D12.18.0-blue.svg)
[![Licencia: EUPL--1.2](https://img.shields.io/badge/License-EUPL--1.2-yellow.svg)](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)

> desy-html-templates es una colección de plantillas de página para maquetar aplicaciones web para el Gobierno de Aragón. Este proyecto usa la librería desy-html, para poder usar los componentes del sistema de diseño. Las plantillas están realizadas en Nunjucks, que es el lenguaje de plantillas que usa desy-html. La idea es tomar estas plantillas, con sus includes correspondientes y copiarlos-pegarlos en un nuevo proyecto creado a partir de desy-html-starter.

## Sitio web
[https://desy.aragon.es/](https://desy.aragon.es/)

## Repositorio
[https://bitbucket.org/sdaragon/desy-html-templates/](https://bitbucket.org/sdaragon/desy-html-templates/)

## Prerrequisitos

- npm >=6.14.0
- node >=12.18.0

## Instalación

```sh
npm install
```

## Primeros pasos

Descárgate o haz un fork del repositorio [desy-html-starter](https://bitbucket.org/sdaragon/desy-html-starter/). Personalízalo cambiando el nombre y, si lo necesitas, las rutas de compilación. 

Descárgate este repositorio y toma los archivos que necesites de la carpeta `src/templates/pages`, o bien de la carpeta `src/templates`. 

Utiliza cualquier archivo de template (Ej.: `_template.logged.njk`) como template de página a la hora de crear tu primera página maquetada. Puedes ver cómo utilizar los archivos de plantilla mirando el código Nunjucks de cualquier página (Ej. : `src/plantilla-logueado-con-titulo-de-app.html`). 

Para compilar el proyecto para producción en la carpeta /dist: compila el HTML, CSS purgeado y minificado para producción y Javascript:

```sh
npm run prod
```

Para desarrollar el proyecto: compila el HTML, CSS con todas las clases de Tailwind CSS y Javascript y escucha cambios en los archivos con Browser sync. Para ver el resultado abre una ventana del navegador e introduce la dirección http://localhost:3000/

```sh
npm run dev
```

## Autor

SDA Servicios Digitales de Aragón


## Licencia

Este proyecto tiene la licencia [EUPL--1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)