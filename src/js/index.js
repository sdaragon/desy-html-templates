// Dependencies for dropdownComponent and listboxComponent:


import {
  accordionComponent,
  alertComponent,
  collapsibleComponent,
  dialogComponent,
  dropdownComponent,
  listboxComponent,
  menubarComponent,
  menuNavigationComponent,
  tableAdvancedComponent,
  tabsComponent,
  tooltipComponent,
  toggleComponent,
  treeComponent,
  notificationComponent,
  radioButtonComponent,
  checkBoxComponent
} from './desy-html.js';

var aria = aria || {};

accordionComponent(aria);
alertComponent(aria);
collapsibleComponent(aria);
dialogComponent(aria);
dropdownComponent(aria);
listboxComponent(aria);
menubarComponent(aria);
menuNavigationComponent(aria);
tableAdvancedComponent(aria);
tabsComponent(aria);
toggleComponent(aria);
tooltipComponent(aria);
treeComponent(aria);
notificationComponent(aria);
radioButtonComponent(aria);
checkBoxComponent(aria);
